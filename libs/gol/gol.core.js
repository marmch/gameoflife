/**
 * Game Of Life Core 
 *
 * @author David Ansermot
 * @date 2014-04-10
 * @package game_of_life
 */

/**
 * @var obj Game of Life root object
 */
var GoL = GoL || {};



/**
 * Core related methods
 */
GoL.core = {

	// Cells storing
	currentGeneration: [],

	// Animation variable
	isRunning: false,
	generations: 0,
	livingCells: 0,
	interval: 0.2,
	timer: null,

	// DOM properties
	worldHandler: '#arena',
	runButtonHandler: '#run-button',
	resetButtonHandler: '#reset-button',
	settingsButtonHandler: '#settings-button',
	saveButtonHandler: '#save-button',
	closeButtonHandler: '#close-button',
	generationsLabelHandler: '#generation-value',
	cellsLabelHandler: '#cells-value',
	optionsPageHandler: '#options-page',
	cssStylesID: 'gameofliferaw',

	// World properties
	worldSize: 40,
	percentForLife: 80,

	

	/**
	 * Generate a random world
	 *
	 * @param void
	 * @return void
	 */
	generateWorld: function() {

		// Initialisation des cases du tableau
	    if (GoL.core.percentForLife > 0) {

	    	var random = 0;

	    	// Rows
		    for (i = 0, maxI = GoL.core.worldSize; i < maxI; i++) {

		    	// Create empty array in cell to simulate multi-dimensional arrays
				if (GoL.core.currentGeneration[i] == 'undefined' || GoL.core.currentGeneration[i] == null) {
					GoL.core.currentGeneration[i] = [];
				}

				// Columns
				for (j = 0; j < maxI; j++) {

					// Life chance generation
				    random = Math.round(Math.random()*100);

				    // Create cell
					GoL.core.currentGeneration[i][j] = {
				    	id: GoL.core.idForCell(i, j),
				    	alive: true
				    };

				    // Determine if cell is dead
				    if (random <= GoL.core.percentForLife) {
						GoL.core.currentGeneration[i][j].alive = false;
				    }

				    // Add the cell to the DOM
				    GoL.core.addCellToWorld(i, j);

				    // Update living cells counter
				    if (GoL.core.currentGeneration[i][j].alive) {
						GoL.core.livingCells++;
					}
				}
		    }
	    }

	},


	/**
	 * Generate raw css for html header
	 *
	 * @param void
	 * @return void
	 */
	generateGUIrawCSS: function() {

		// Calculate cell side sizw
		var cellSide = $(window).width() / GoL.core.worldSize;
		
		// Generate css
		var css = GoL.core.worldHandler+' { width:'+$(window).width()+'px; height:'+$(window).width()+'px; }';
		css += GoL.core.worldHandler+' .cell { width:'+cellSide+'px; height:'+cellSide+'px; }';
		css += GoL.core.optionsPageHandler+ '{ width:'+$(window).width()+'px; height:'+$(window).height()+'px; }';

		return css;
	},


	/**
	 * Animate each lifecycle
	 *
	 * @param void
	 * @return void
	 */
	animate: function() {

		var currentCell = null;

		// Reset living cells counter
		GoL.core.livingCells = 0;

		// Update current generation
		GoL.core.currentGeneration = GoL.core.createNextGeneration();

		// New generation
		GoL.core.generations++;

		// Update UI
		GoL.core.updateWorld();
		GoL.core.updateGameData();

		// If still running, register next lifecycle
		if (GoL.core.isRunning) {
			GoL.core.timer = window.setTimeout(GoL.core.animate, GoL.core.interval * 1000);
		}
	},


	/**
	 * Add cell html to DOM
	 *
	 * @param int x :Cell row
	 * @param int y :Cell col
	 * @return void
	 */
	addCellToWorld: function(x, y) {

		var cell = GoL.core.currentGeneration[x][y];
		var cellClass = (cell.alive) ? 'alive' : '';

		// Add the html element
		$(GoL.core.worldHandler).append(
			$('<div></div>')
				.attr('id', cell.id)
				.addClass(cellClass)
				.addClass('cell')
		);
	},


	/**
	 * Create the cells next generation array
	 *
	 * @param void
	 * @return array
	 */
	createNextGeneration: function() {

		var nextGeneration = [];

		// Rows
		for (var i = 0; i < GoL.core.worldSize; i++) {

			// Create empty array in cell to simulate multi-dimensional arrays
			if (nextGeneration[i] == 'undefined' || nextGeneration[i] == null) {
				nextGeneration[i] = [];
			}

			// Columns
			for (var j = 0; j < GoL.core.worldSize; j++) {

				// Current cell
				currentCell = GoL.core.currentGeneration[i][j];
			
				// Create cell
				// (by default, the cell stays in the same stat)
				nextGeneration[i][j] = {
					id: 'cell_'+i+'_'+j,
					alive: currentCell.alive
				};

				// Calculate alive neighbors
				neighbors = GoL.core.countNeighbors(i, j);
				
				// Die
				if (neighbors > 3 || neighbors < 2) {
					nextGeneration[i][j].alive = false;
				} else if (neighbors == 3) {
					nextGeneration[i][j].alive = true;
				} 

				// Update living cells counter
				if (nextGeneration[i][j].alive) {
					GoL.core.livingCells++;
				}
			}
		}

		return nextGeneration;
	},


	/**
	 * Update the world's cells
	 *
	 * @param void
	 * @param void
	 */
	updateWorld: function() {

		var cellID = '';

		// Rows
		for (var i = 0; i < GoL.core.worldSize; i++) {

			// Columns
			for (var j = 0; j < GoL.core.worldSize; j++) {
				cellID = GoL.core.idForCell(i, j);

				// Check if cell stat need to change
				if (($('#'+cellID).hasClass('alive') && GoL.core.currentGeneration[i][j].alive == false) ||
					(!$('#'+cellID).hasClass('alive') && GoL.core.currentGeneration[i][j].alive == true)) {
					$('#'+cellID).toggleClass('alive');
				}
			}
		}
	},


	/**
	 * Update all the displayed game data
	 *
	 * @param void
	 * @return void
	 */ 
	updateGameData: function() {
		GoL.core.updateGenerationLabel();
		GoL.core.updateCellsLabel();
	},


	/**
	 * Update the generations label
	 *
	 * @param void
	 * @return void
	 */
	updateGenerationLabel: function() {
		$(GoL.core.generationsLabelHandler).html(GoL.core.generations);
	},


	/**
	 * Update the living cells label
	 *
	 */
	updateCellsLabel: function() {
		var totalCells = Math.pow(GoL.core.worldSize, 2);
		$(GoL.core.cellsLabelHandler).html(GoL.core.livingCells+'/'+totalCells);
	},


	/** 
	 * Update sizes of world and cells based on window size
	 *
	 * @param void
	 * @return void
	 */
	updateGUIsizes: function() {
		var css = GoL.core.generateGUIrawCSS();
		$('#'+GoL.core.cssStylesID).html(css);
	},


	/**
	 * Toggle the cell stat (if alive => dead, if dead => alive)
	 * 
	 * @param int x :Cell row
	 * @param int y :Cell col
	 * @return void
	 */
	toggleCellStat: function(x, y) {

		var cell = GoL.core.currentGeneration[x][y];

		$('#'+cell.id).toggleClass('alive');
		cell.alive = !cell.alive;
	},


	/**
	 * Count alive neighbors of a cell
	 *
	 * @param int x :Cell row
	 * @param int y :Cell col
	 * @return int
	 */
	countNeighbors: function(x, y) {

		count = 0;

		var modA = 0;
		var modB = 0;

		// Check from left cell to right cell
		for (var a = (x - 1); a <= (x + 1); a++) {

			// Check from top cell to bottom cell
			for (var b = (y - 1); b <= (y + 1); b++) {

				// Prevent to check self
				if ((a != x) || (b != y)) {

					// Need to consider that the array is a ring
					modA = (a + GoL.core.worldSize) % GoL.core.worldSize;
					modB = (b + GoL.core.worldSize) % GoL.core.worldSize;

					if (GoL.core.currentGeneration[modA][modB].alive == true) {
						count++;
					}
				}

				// Prevent to continue for nothing
				if (count >= 4) {
					break;
				}
			}
		}

		return count;
	},


	/**
	 *	Create the cell id from cell indexes
	 *	
	 * @param int x :Cell row
	 * @param int y :Cell col
	 * @return string
	 */
	idForCell: function(x, y) {
		return 'cell_'+x+'_'+y;
	}

};
