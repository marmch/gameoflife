/**
 * Game Of Life
 *
 * @author David Ansermot
 * @date 2014-04-09
 */

/**
 * @var obj Game of Life root object
 */
var GoL = GoL || {};


/**
 * Initialisation root method
 *
 * @param void
 * @return void
 */
GoL.init = function() {

	// Attach DOM events
	GoL.events.attachEvents();

	// Create raw styles
	var css = GoL.core.generateGUIrawCSS();
	$('head').append(
		$('<style id="'+GoL.core.cssStylesID+'"></style>')
			.html(css)
	);

	// Hide options page
	$(GoL.core.optionsPageHandler).hide().css('top', $(window).height()+'px');

	// Reset "game"
	GoL.reset();
};


/**
 * (Re)Start the lifecycle
 *
 * @param void
 * @return void
 */
GoL.start = function() {
	GoL.core.isRunning = true;
	GoL.core.animate();

	$(GoL.core.runButtonHandler).html('<span class="glyphicon glyphicon-stop"></span>&nbsp;Stop')
								.removeClass('btn-success')
								.addClass('btn-danger');
};


/**
 * Stop the lifecycle
 *
 * @param void
 * @return void
 */
GoL.stop = function() {
	GoL.core.isRunning = false;

	// Clear the lifecycle timer
	window.clearTimeout(GoL.core.timer);

	$(GoL.core.runButtonHandler).html('<span class="glyphicon glyphicon-play"></span>&nbspStart')
								.removeClass('btn-danger')
								.addClass('btn-success');
};


/**
 * Reset the game stats
 *
 * @param void
 * @return void
 */
GoL.reset = function() {

	// Stops if running
	if (GoL.core.isRunning) {
		GoL.stop();
	}

	// Remove html cells
	$(GoL.core.worldHandler+' .cell').remove();

	// Reset counters
	GoL.core.generations = 0;
	GoL.core.livingCells = 0;

	// Reset cells
	GoL.core.currentGeneration = [];

	// Generate world
	GoL.core.generateWorld();

	// Update generation label
	GoL.core.updateGameData();
};


/**
 * Display options page
 *
 * @param void
 * @return void
 */
GoL.showSettings = function() {
	// Stop game if running
	if (GoL.core.isRunning) {
		GoL.stop();
	}

	// Update page values from current context
	$(GoL.core.optionsPageHandler).find('#opt-worldsize').find('option').filter(function() {
	    return $(this).attr('value') == GoL.core.worldSize; 
	}).prop('selected', true);
	$(GoL.core.optionsPageHandler).find('#opt-interval').attr('value', GoL.core.interval);
	$(GoL.core.optionsPageHandler).find('#opt-percentoflife').attr('value', GoL.core.percentForLife);

	// Display page
	$(GoL.core.optionsPageHandler).show();
	$(GoL.core.optionsPageHandler).animate({
		'top': '0'
	}, 200);
};


/**
 * Close options page
 *
 * @param void 
 * @return void
 */
GoL.closeSettings = function() {
	$(GoL.core.optionsPageHandler).animate({
		'top': $(window).height()+'px'
	}, 
	200,
	function() {
		$(GoL.core.optionsPageHandler).hide();
	});
};


/**
 * Update the options 
 *
 * @param void
 * @return void
 */
GoL.saveSettings = function() {

	// World Size
	GoL.core.worldSize = parseInt($(GoL.core.optionsPageHandler).find('#opt-worldsize').val());

	// Interval
	if ($(GoL.core.optionsPageHandler).find('#opt-interval').attr('value') != '') {
		GoL.core.interval = parseFloat($(GoL.core.optionsPageHandler).find('#opt-interval').val());
	}

	// Degree of life
	if ($(GoL.core.optionsPageHandler).find('#opt-percentoflife').attr('value') != '') {
		GoL.core.percentForLife = parseInt($(GoL.core.optionsPageHandler).find('#opt-percentoflife').val());
	}
};
