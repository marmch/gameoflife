/**
 * Game Of Life Events 
 *
 * @author David Ansermot
 * @date 2014-04-10
 * @package game_of_life
 */

/**
 * @var obj Game of Life root object
 */
var GoL = GoL || {};



/**
 * Events related methods
 */
GoL.events = {

	/**
	 * Attach all events on the DOM
	 *
	 * @param void
	 * @return void
	 */
	attachEvents: function() {
		$(GoL.core.runButtonHandler).click(GoL.events.playButtonClicked);
		$(GoL.core.resetButtonHandler).click(GoL.events.resetButtonClicked);
		$(GoL.core.settingsButtonHandler).click(GoL.events.settingsButtonClicked);
		$(GoL.core.saveButtonHandler).click(GoL.events.saveButtonClicked);
		$(GoL.core.closeButtonHandler).click(GoL.events.closeButtonClicked);
		window.addEventListener('orientationchange', GoL.events.orientationChanged);
	},



	//
	// Callbacks
	//

	/**
	 * Play button clicked
	 *
	 * @param void
	 * @return void
	 */
	playButtonClicked: function() {

		if (GoL.core.isRunning) {
			GoL.stop();
		} else {
			GoL.start();
		}

	},


	/**
	 * Reset button clicked
	 *
	 * @param void
	 * @return void
	 */
	resetButtonClicked: function() {
		GoL.reset();
	},


	/**
	 * Settings button clicked
	 *
	 * @param void
	 * @return void
	 */
	settingsButtonClicked: function() {
		GoL.showSettings();
	},


	/**
	 * Save button clicked
	 *
	 * @param void
	 * @return void
	 */
	saveButtonClicked: function() {

		// Save settings
		GoL.saveSettings();

		// Reload game
		GoL.reset();

		// Update sizes
		GoL.core.updateGUIsizes();

		// Close window
		GoL.closeSettings();
	},


	/**
	 * Close button clicked
	 *
	 * @param void
	 * @return void
	 */
	closeButtonClicked: function() {
		GoL.closeSettings();
	},


	/**
	 * Window orientation changed
	 *
	 * @param void
	 * @return void
	 */
	orientationChanged: function(e) {
		GoL.core.updateGUIsizes();
	}
};
